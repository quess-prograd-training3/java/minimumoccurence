package com.example;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Occurences {
    String str;

    public Occurences(String str) {
        this.str = str;
    }

    public void countOccurancesOfO(){
        int count=0;
        for (int irerate = 0; irerate < str.length(); irerate++) {
            if(str.charAt(irerate)=='o'){
                count++;
            }
        }
        System.out.println("Number of O occurances= "+count);
    }

    public void characterOccurances(){
        Map<Character,Integer> map=new LinkedHashMap<>();
        for (int iterate = 0; iterate < str.length(); iterate++) {
            if(str.charAt(iterate) != ' ') {
                if (map.containsKey(str.charAt(iterate))) {

                    map.put(str.charAt(iterate), map.get(str.charAt(iterate))+1);
                } else {

                    map.put(str.charAt(iterate), 1);
                }
            }
        }
        System.out.println(map.size());
        int count=Integer.MAX_VALUE;
        char ch = 0;
        for (Map.Entry<Character,Integer> result: map.entrySet()) {
            if(result.getValue() < count){
                count=result.getValue();
                ch=result.getKey();
            }

        }
        System.out.println("Minimum occurance caharacter: "+ch+" "+count+" times");

    }
}


